const process = (input) => {
    const output = [input[0]];
    input.splice(0, 1);

    while (input.length !== 0) {
        const lastValue = output[output.length - 1];
        let bestScore;
        let bestIndex;

        input.splice(0, 1);
        input.forEach((val, index) => {
            const score =  val - lastValue + index;

            if (lastValue > val) {
                return;
            }

            if (!bestScore || (score <= bestScore)) {
                bestScore = score;
                bestIndex = index;
            }
        });

        if (!bestScore && bestIndex) {
            throw new Error()
        }

        output.push(input[bestIndex]);
        input.splice(0, bestIndex + 1);
    }

    return output;
};

const input = [1, 9, 5, 13, 3, 11, 7, 15, 2, 10, 6, 14, 4, 12, 8, 16];
const output = process(input);
//
console.log("success:", JSON.stringify(output) === JSON.stringify([1, 3, 7, 10, 12, 16]));
console.log("output:", output);